      program integration_sequential
!
      IMPLICIT NONE
!
      real      a        ! Left endpoint.
      real      b        ! Right endpoint.
      integer   n        ! Number of trapezoids.
      real      h        ! Trapezoid base length.
      real      total    ! Total integral.
      real      local_a  ! Left endpoint for my process.
      real      local_b  ! Right endpoint my process.
      integer   local_n  ! Number of trapezoids for my segment
      real      Trap
      real      Midpoint
      real      Simpson
      real      start, finish, total_time

      data a, b, n /0.0, 1.0, 1e6/

      h = (b-a)/n        ! h is the same for all processes.

            write(6,100) n
 100        format(' ','Number of divisions n = ',I10)
            write(6,101) a, b
 101        format(' ','Integral from',f6.2,' to',f6.2)

      call cpu_time(start)
      total = Midpoint(a, b, n, h);
      call cpu_time(finish)
      total_time = finish - start

      write (6,*) ''
      write (6,*) 'Midpoint Rule Results'
      write(6,200) total, total_time
 200        format(' ','Integral equal:',f12.8,' Taken time:',f11.8)
 
 call cpu_time(start)
      total = Trap(a, b, n, h);
      call cpu_time(finish)
      total_time = finish - start

      write (6,*) ''
      write (6,*) 'Trapezoidal Rule Results'
      write(6,300) total, total_time
 300        format(' ','Integral equal:',f12.8,' Taken time:',f11.8)
 
 call cpu_time(start)
      total = Simpson(a, b, n, h);
      call cpu_time(finish)
      total_time = finish - start

      write (6,*) ''
      write (6,*) 'Simpson Rule Results'
      write(6,400) total, total_time
 400        format(' ','Integral equal:',f12.8,' Taken time:',f11.8)

      end program integration_sequential


      real function Trap(local_a, local_b, local_n, h)
      IMPLICIT NONE
      real     local_a
      real     local_b
      integer  local_n
      real     h 
      real     integral    ! Store result in integal.
      real     x 
      integer  i 
      
      real     f
      integral = (f(local_a) + f(local_b))/2.0 
      x = local_a 
      do i = 1,local_n-1
        x = x + h;
        integral = integral + f(x);
      enddo  
      integral = integral*h 
      Trap = integral
      return
      end
      
      real function Midpoint(local_a, local_b, local_n, h)
      IMPLICIT NONE
      real     local_a
      real     local_b
      integer  local_n
      real     h 
      real     integral    ! Store result in integal.
      real     x 
      integer  i 
      
      real     f
      x = local_a - (h / 2.0);
      integral = 0.0;
      do i = 1,local_n
        x = x + h
        integral = integral + f(x) 
      enddo  
      integral = integral*h 
      Midpoint = integral
      return
      end
      
      real function Simpson(local_a, local_b, local_n, h)
      IMPLICIT NONE
      real     local_a
      real     local_b
      real     x_current
      real     x_next
      integer  local_n
      real     h 
      real     integral    ! Store result in integal.
      real     x 
      integer  i 
      
      real     f
      x_current = local_a;
      integral = 0.0;
      do i = 1,local_n
        x_next = x_current + h
        integral=integral+(1.0/6.0)*(f(x_current)+4.0*f((x_current+x_next)/2.0)+f(x_next))
        x_current = x_next
      enddo  
      integral = integral*h 
      Simpson = integral
      return
      end
    
      real function f(x)
      IMPLICIT NONE
      real x
      f=4/(1+x*x)
      end
