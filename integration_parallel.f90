      program trapezoidal
!
      IMPLICIT NONE
      include 'mpif.h'
!
      integer   my_rank  ! My process rank.
      integer   p        ! The number of processes.
      real      a        ! Left endpoint.
      real      b        ! Right endpoint.
      integer   n        ! Number of trapezoids.
      real      h        ! Trapezoid base length.
      real      local_a  ! Left endpoint for my process.
      real      local_b  ! Right endpoint my process.
      integer   local_n  ! Number of trapezoids for my 
                         ! calculation.
      real      integral ! Integral over my interval.
      real      total    ! Total integral.

      real      Trap
      real      Midpoint
      real      Simpson
      real      start, finish, total_time

      integer   source   ! Process sending integal.
      integer   dest     ! All messages go to 0.
      integer   tag     
      integer   status(MPI_STATUS_SIZE)
      integer   ierr

      data a, b, n, dest, tag /0.0, 1.0, 1024, 0, 50/

!  Let the system do what it needs to start up MPI.	
      call MPI_INIT(ierr)

!  Get my process rank.
      call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank, ierr)

!  Find out how many processes are being used.
      call MPI_COMM_SIZE(MPI_COMM_WORLD, p, ierr)
      h = (b-a)/n        ! h is the same for all processes.
      local_n = n/p      ! So is the number of trapezoids.

      if (my_rank .EQ. 0) then
            write(6,100) n
 100        format(' ','Number of divisions n = ',I10)
            write(6,101) a, b
 101        format(' ','Integral from',f6.2,' to',f6.2)
      endif
!  Length of each process' interval of integration = local_n*h.
!  So my interval starts at :	
      local_a = a + my_rank*local_n*h
      local_b = local_a + local_n*h

      if (my_rank .EQ. 0) then
          call cpu_time(start)
      endif

      integral = Trap(local_a, local_b, local_n, h)

!  Add up the integals calculated by each process.
      if (my_rank .EQ. 0) then
          total = integral
          do source = 1, p-1
              call MPI_RECV(integral, 1, MPI_REAL, source, tag,&
                   MPI_COMM_WORLD, status, ierr)
              total = total + integral
          enddo
          call cpu_time(finish)
          total_time = finish - start
      else
          call MPI_SEND(integral, 1, MPI_REAL, dest,&
              tag, MPI_COMM_WORLD, ierr)
      endif

!  Print the result.
      if (my_rank .EQ. 0) then
          write (6,*) ''
          write (6,*) 'Trapezoidal Rule Results'
          write(6,300) total, total_time
 300        format(' ','Integral equal:',f12.8,' Taken time:',f11.8)
      endif

!       Shut down MPI.
      call MPI_FINALIZE(ierr) 
      end program trapezoidal


      real function Trap(local_a, local_b, local_n, h)
      IMPLICIT NONE
      real     local_a
      real     local_b
      integer  local_n
      real     h 
      real     integral    ! Store result in integal.
      real     x 
      integer  i 
      
      real     f
      integral = (f(local_a) + f(local_b))/2.0 
      x = local_a 
      do i = 1,local_n-1
        x = x + h;
        integral = integral + f(x);
      enddo  
      integral = integral*h 
      Trap = integral
      return
      end
      
      real function Midpoint(local_a, local_b, local_n, h)
      IMPLICIT NONE
      real     local_a
      real     local_b
      integer  local_n
      real     h 
      real     integral    ! Store result in integal.
      real     x 
      integer  i 
      
      real     f
      x = local_a - (h / 2.0);
      integral = 0.0;
      do i = 1,local_n
        x = x + h
        integral = integral + f(x) 
      enddo  
      integral = integral*h 
      Midpoint = integral
      return
      end
      
      real function Simpson(local_a, local_b, local_n, h)
      IMPLICIT NONE
      real     local_a
      real     local_b
      real     x_current
      real     x_next
      integer  local_n
      real     h 
      real     integral    ! Store result in integal.
      real     x 
      integer  i 
      
      real     f
      x_current = local_a;
      integral = 0.0;
      do i = 1,local_n
        x_next = x_current + h
        integral=integral+(1.0/6.0)*(f(x_current)+4.0*f((x_current+x_next)/2.0)+f(x_next))
        x_current = x_next
      enddo  
      integral = integral*h 
      Simpson = integral
      return
      end
    
      real function f(x)
      IMPLICIT NONE
      real x
      f=4/(1+x*x)
      end
